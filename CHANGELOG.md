
Version 1.5.0
==============

- release with changelog
- Adding chill doc store bundle to basic repo

Master branch
=============

- move notification sender identity to parameters
- the executable `docker-node.sh` is now executable directly (`./docker-node.sh`) and will run under current uid and gid


Things you must do
------------------

Change the owner on your directory "build":

```
sudo chown $(id -u):$(id -g) -R web/build
sudo chown $(id -u):$(id -g) -R node_modules
sudo chown $(id -u):$(id -g)    yarn.lock
sudo chown $(id -u):$(id -g)    yarn-error.log
sudo chown $(id -u):$(id -g) -R .yarncache

```

