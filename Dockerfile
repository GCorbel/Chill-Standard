FROM php:7.2-fpm

MAINTAINER Julien Fastré <julien.fastre@champs-libres.coop>

ENV PHPREDIS_VERSION 4.0.0

# Set the lifetime of a PHP session
ARG SESSION_LIFETIME=10800 
# default UID for the PHP user
ARG UID=1000

RUN apt update && apt -y --no-install-recommends install wget gnupg  \
   && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc |  apt-key add - \
   && echo "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main" > /etc/apt/sources.list.d/pgdg.list \
   && apt update && apt -y --no-install-recommends install \
      libicu-dev  \
      g++ \
      postgresql-server-dev-10 \
      libzip-dev \
      libfreetype6-dev \
      libpng-dev \
      libjpeg62-turbo-dev \
      git \
      unzip \
   && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
   && docker-php-ext-install -j$(nproc) gd \
   && docker-php-ext-install intl pdo_pgsql mbstring zip bcmath sockets exif \
   && mkdir /tmp/redis \
   && curl -L -o /tmp/redis/redis.tar.gz https://github.com/phpredis/phpredis/archive/$PHPREDIS_VERSION.tar.gz \
   && cd /tmp/redis \
   && tar xfz /tmp/redis/redis.tar.gz \
   && rm -r /tmp/redis/redis.tar.gz \
   && mkdir -p /usr/src/php/ext/redis \
   && mv /tmp/redis/phpredis-$PHPREDIS_VERSION/* /usr/src/php/ext/redis/. \
   && docker-php-ext-install redis \
   && apt remove -y wget libicu-dev g++ gnupg libzip-dev \
   && apt autoremove -y \
   && apt purge -y


RUN { \
    echo ""; \
    echo "[Date]"; \
    echo "date.timezone = Europe/Brussels"; \
    echo ""; \
    } >> /usr/local/etc/php/conf.d/date.ini

# register session in redis and store password
RUN { \
    echo ""; \
    echo "session.save_handler = redis" ; \
    echo "session.save_path = \"tcp://redis:6379?db=10\"" ; \
    echo "session.gc_maxlifetime = ${SESSION_LIFETIME}" ; \
    } >> /usr/local/etc/php/conf.d/custom.ini

WORKDIR /var/www/app

COPY ./app    /var/www/app/app/.
COPY ./bin    /var/www/app/bin/.
COPY ./src    /var/www/app/src/.
COPY ./vendor /var/www/app/vendor/.
COPY ./web    /var/www/app/web/.
COPY ./composer.* /var/www/app/

# import the manifest.json file
COPY ./web/build/manifest.json /var/www/app/web/build/manifest.json

ADD ./entrypoint.sh /.

RUN chmod u+x /entrypoint.sh
#   && chown www-data:www-data var/logs -R \
#   && chown www-data:www-data var/cache -R

# ajoute des utilisateurs ayant le uid 1000
RUN useradd --uid ${UID} --create-home "user${UID}"

ENV PHP_FPM_USER=www-data \
    PHP_FPM_GROUP=www-data \
    GELF_HOST=gelf \
    GELF_PORT=12201 \
    APP_DEBUG=false \
    APP_ENV=prod \
    DATABASE_HOST=db \
    DATABASE_NAME=postgres \
    DATABASE_PORT=5432 \
    DATABASE_USER=postgres \
    COMPOSER_HOME=/var/www/app/.composer


ENTRYPOINT ["/entrypoint.sh"]

CMD ["php-fpm"]

