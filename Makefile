THIS_FILE := $(lastword $(MAKEFILE_LIST))
PWD:=$(shell echo ${PWD})
UID:=$(shell id -u)
GID:=$(shell id -g)
DOCKERNODE_CMD=docker run --rm --user ${UID}:${GID} -v ${PWD}:/app --workdir /app -e YARN_CACHE_FOLDER=/app/.yarncache node:9
DOCKER_COMPOSE_PHP_EXEC_CMD=docker-compose run --user $(UID) --entrypoint /usr/bin/env php
PHP_BASE_IMAGE=php:7.2-fpm
NGINX_BASE_IMAGE=nginx

help:
	@echo "Please make use of 'make <target>' where <target> is one of: "
	@echo "  build-assets           to build assets for production"
	@echo "  build-and-push-images  to build image PHP and NGINX and push them on a registry"
	@echo "  init                   to initialize your development directory"
	@echo "  install-vendor         to install vendor using composer"
	@echo "  pull-base-image        to pull base image used in image php and nginx"
	@echo "  upgrade-vendors        to upgrade vendor"

build-assets:
	$(DOCKERNODE_CMD) yarn install
	$(DOCKERNODE_CMD) yarn run encore production

init:
	docker run --rm -e INSTALL_ID=$(UID) -e INSTALL_GID=$(GID) -e COMPOSER_HOME=/var/www/app/.composer --volume $(PWD):/var/www/app --workdir /var/www/app php:7.2-cli /bin/bash ./docker/install.dev.sh
	@$(MAKE) -f $(THIS_FILE) build-assets

push-images:
	docker-compose push php nginx

pull-base-images:
	docker pull $(PHP_BASE_IMAGE)
	docker pull $(NGINX_BASE_IMAGE)

build-and-push-images:
	@$(MAKE) -f $(THIS_FILE) build-assets
	docker-compose build php nginx 
	@$(MAKE) -f $(THIS_FILE) push-images

upgrade-vendors:
	$(DOCKER_COMPOSE_PHP_EXEC_CMD) php composer.phar upgrade

install-vendors:
	$(DOCKER_COMPOSE_PHP_EXEC_CMD) php composer.phar install
