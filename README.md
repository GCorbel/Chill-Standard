Chill - Standard Project
========================

The Chill project is a fork of [symfony/symfony-standard]
(https://github.com/symfony/symfony-standard). This project has adapted the composer.json
file to adapt to chill installation.

## Requirements

- This project use [docker](https://docker.com) to be run. As a developer, use [docker-compose](https://docs.docker.com/compose/overview/) to bootstrap a dev environment in a glance. You do not need any other dependencies ;
- Make is used to automate scripts.

## As a developer

Clone or download this project and `cd` into the main directory.

As a developer, the code will stay on your computer and will be executed in docker container. To avoid permission problem, the code should be run with the same uid/gid from your current user. Have a look at those id before continuing.

### Prepare your variables

Have a look at the variable in [`env.dist`](env.dist) and in [`app/config/parameters.yml.dist`](app/config/parameters.yml.dist) and check if you need to adapt them. If they do not adapt with your need, or if some are missing: 

1. copy the file as `.env`: `cp env.dist .env`
2. replace the variable inside `.env`
3. copy the file `parameters.yml.dist`: `cp app/config/parameters.yml.dist app/config/parameters.yml`
4. replace eventually the variable inside this file

**Note**: If you intend to use the bundle `Chill-Doc-Store`, you will need to configure and install an openstack object storage container with temporary url middleware. You will have to configure [secret keys](https://docs.openstack.org/swift/latest/api/temporary_url_middleware.html#secret-keys).

### Run the bootstrap script

This script can be run using `make`

```bash
make init
```

This script will :

1. force docker-compose to, eventually, pull the base images and build the image used by this project ;
2. run an install script to download [`composer`](https://getcomposer.org) ; 
3. install the php dependencies

### Build assets

run those commands: 

```
make build-assets
```

### Start the project

```bash
docker-compose up
```

**On the first run** (and after each upgrade), you must execute database migrations:

```
docker-compose exec --user <uid> php app/console doctrine:migrations:migrate
```

Chill will be available at http://localhost:8001. Currently, there isn't any user or data. To add fixtures, run

```
docker-compose exec --user <uid> php app/console doctrine:fixtures:load
```

There are several users available:

- `center a_social`
- `center b_social`

The password is always `password`.

Now, read the how-tos.

## How to

### How to read the email sent by the program ?

Go at http://localhost:8005 and you should have access to mailcatcher.

In case of you should click on a link in the email, be aware that you should remove the "s" from https.

### How to execute the console ?

```
# if a container is running
docker-compose exec --user <uid> php app/console
# if not 
docker-compose run --user <uid> php app/console
```

### How to create the database schema (= run migrations) ?

```
# if a container is running
docker-compose exec --user <uid> php app/console doctrine:migrations:migrate
# if not 
docker-compose run --user <uid> php app/console doctrine:migrations:migrate
```

### How to load fixtures ?

```
# if a container is running
docker-compose exec --user <uid> php app/console doctrine:fixtures:load
# if not 
docker-compose run --user <uid> php app/console doctrine:fixtures:load
```

### How to open a terminal in the project

```
# if a container is running
docker-compose exec --user <uid> php /bin/bash
# if not 
docker-compose run --user <uid> php /bin/bash
```

### How to run composer ?

```
# if a container is running
docker-compose exec --user <uid> php ./composer.phar
# if not 
docker-compose run --user <uid> php ./composer.phar
```

### How to access to PGADMIN ?

Pgadmin is installed with docker-compose.

You can access it at http://localhost:8002 .

Credentials:

- login: admin@chill.social
- password: password

### How to run tests ?

Tests reside inside the installed bundles. You must `cd` into that directory, download the required packages, and execute them from this place.

**Note**: some bundle require the fixture to be executed. See the dedicated _how-tos_.

Exemple, for running test inside `main` bundle: 

```
# mount into the php image
docker-compose run --user <uid> php /bin/bash
# cd into main directory
cd vendor/chill-project/main
# download deps
php ../../../composer.phar install
# run tests
/vendor/bin/phpunit
```

### How to run webpack interactively

Executing `bash docker-node.sh` will open a terminal in a node:9 container, with volumes mounted.

## Running in production

TODO

## Design principles

### Why the DB URL is set in environment, and not in parameters.yml ?

Because, at startup, a script does check the db is up and, if not, wait for a couple of seconds before running [`entrypoint.sh`](entrypoint.sh). For avoiding double configuration, the configuration of the PHP app takes his configuration from environment also (and it will be standard in future releases, with symfony 4.0).
