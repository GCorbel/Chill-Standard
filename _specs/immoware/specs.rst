################
Immoware > CHILL
################


.. list-table:: 

   * - Contact technique AMLI à mobiliser prioritairement
     - Grégory Simonet <gregory.simonet@batigere.fr>
   * - Auteur et contact technique Champs-Libres
     - Julien Fastré <julien.fastre@champs-libres.coop>

Historique des versions
***********************

.. list-table::
   
   * - Version 1 (actuelle)
     - 20 avril 2018
     - Document initial


********
Contexte
********

AMLI réalise des prestations de diagnostic social pour les utilisateurs d'Immoware. Dans ce cadre, les utilisateurs d'Immoware transmettent des informations sur leurs clients à AMLI.

AMLI développe un système d'informations sur les procédures de diagnostic social. Un extranet est disponible pour suivre ces procédures.

Le but de ce développement est de permettre l'automatisation des échanges entre Immoware et le SI AMLI.

=========
Objectifs
=========

- Permettre aux utilisateurs d'Immoware de transmettre des informations clients pour les exploiter dans le système d'information AMLI ;
- Permettre aux utilisateurs d'Immoware de suivre les procédures externes renseignées dans le système d'information AMLI.

======================
Expérience utilisateur
======================

L'utilisateur d'Immoware dispose d'un ou plusieurs boutons "Mandater AMLI" dans le logiciel. En cliquant sur ce bouton, il est renvoyé vers l'URL spécifique qui lui permet de renseigner les informations nécessaires à AMLI pour travailler. Ce formulaire est pré-rempli avec les données issues d'Immoware.

Si l'utilisateur n'a pas de session authentifiée ouverte, le système d'information AMLI présentera un écran de conenxion, puis redirigera vers la page pré-remplie.

Après avoir introduit la demande de procédure sociale à l'AMLI, la demande est encodée dans le SI AMLI dans l'écran PAGE 3 (TODO). Lorsqu'AMLI a terminé certains diagnostics, des informations de base sont visibles dans Immoware, sans avoir à ré-ouvrir le SI d'AMLI.

Emplacement des boutons
=======================

Un premier bouton serait à placer sur la page d'accueil de Immoware

.. figure:: emplacement_button_1_avec_dessin.png
   :figwidth: 100%

   Bouton pour créer une demande de procédure sociale, sur la page d'accueil de Immoware.

Un second bouton serait placé dans le dossier du client:


.. figure:: emplacement_button_2_avec_dessin.png
   :scale: 100 %

   Bouton pour créer une demande de procédure sociale, depuis le dossier du client

.. figure:: indication_procedure_avec_dessin.png
   :scale: 100 %

   Indication de la procédure en cours dans immoware.

Le logo de Chill à utiliser peut être fourni au format svg ou png.

==========================================
Informations à transmettre depuis Immoware
==========================================

Pour la personne concernée :

- nom du client ;
- prénom du client ;
- date de naissance ;
- numéro contrat locataire ;
- situation familiale (si la personne est célibataire, pacsée, mariée, etc.) 
- adresse ;
- genre ;
- adresse email ;
- téléphone mobile ;
- téléphone fixe ;

S'il existe un co-demandeur, ces mêmes informations doivent être transmises.

S'il existe des autres personnes dans le ménage de la personne concernée (enfants, etc.), pour chaque personne :

- prénom ;
- nom ;
- genre ;
- date de naissance ;
- lien de parenté ;
- situation familiale ;

(liste principale, à éventuellement compléter)

*************************
Spécifications techniques
*************************

===============================
Système d'information de l'AMLI 
===============================

Le système d'information AMLI est une application web hébergée en https. Les utilisateurs d'Immoware y disposent d'un accès sécurisé par login et par mot de passe. 

Ce système d'information est installé sur un seul serveur. Deux instances sont prévues, aux adresses :

- chill.amli.asso.fr pour le système en production ;
- chill-test.amli.asso.fr pour une instance de staging / test.

=============================
Transmission de l'information
=============================

Les informations seront transmises :

- dans le meilleur des cas, par requête POST depuis Immoware vers le système d'information. Le système d'information répondra avec l'url de redirection pour l'utilisateur ;
- sinon, par requête GET vers le système d'information AMLI ;


L'uri à utiliser sera la suivante : :code:`/external/amli/mandatement`.

Les informations seront à transmettre en paramètres :

- de la requête :code:`POST`, dans le body de la requête HTTP ;
- de la requête :code:`GET`, dans l'URL de la requête HTTP ;

Les spécifications HTTP (encodage des caractères UTF-8, etc.) doivent être respectées.

Les paramètres seront les suivants :

Coordonnées de la personne concernée
------------------------------------

.. list-table:: Paramètres
   :header-rows: 1

   * - Nom du paramètre
     - Information transmise
     - Exemple éventuel
     - Requis (R) ou optionnel (O)
   * - p[0][name]
     - Nom de famille du principal demandeur
     - 
     - R
   * - p[0][firstname]
     - Prénom du principal demandeur
     - 
     - R
   * - p[0][birthdate]
     - Date de naissance, au format ISO8601
     - :code:`p[0]birthdate]=1981-06-16` pour le 16 juin 1981
     - R
   * - p[0][idcontrat]
     - Numéro du contrat locataire
     - 
     - O
   * - p[0][addr][lg1]
     - Adresse, ligne 1
     - 
     - O, mais requis si un code postal (:code:`cp`) est introduit.
   * - p[0][addr][lg2]
     - Adresse, ligne 2
     - 
     - O, mais ne peut pas être présent si :code:`lg1` et `cp` sont introduits.
   * - p[0][addr][cp]
     - Adresse, code postal
     -
     - O, mais requis si :code:`lg1` est introduit.
   * - p[0][gender]
     - Genre de la personne. 
       Enum entre:
       - :code:`m` pour le genre masculin
       - :code:`f` pour le genre féminin
       - :code:`i` pour le genre indéterminé
     - 
     - R
   * - p[0][email][0]
     - Adresse email de la personne.

       Note: les adresse emails peuvent être multiples. Dans ce cas, incrémenter l'index de l'adresse email: :code:`p[0][email][1]` pour la seconde, etc.
     -
     - O
   * - p[0][phone][0][num]
     - Téléphone de la personne. Indiquer le formation international
     - :code:`+336123456789`
     - O 
   * - p[0][phone][0][type]
     - Type de numéro de téléphone. A choisir entre:
       - :code:`m` pour les téléphones portables ;
       - :code:`l` pour les téléphones fixes ;
       
       L'index du type de numéro et du numéro doivent correspondre. 
     - O, mais, si présent, un champ :code:`num` avec le même index doit être présent.


Coordonnées du co-demandeur
---------------------------

Ces informations sont à dupliquer avec l'index 1 pour un éventuel co-demandeur :

- :code:`p[1][name]` pour le nom du demandeur, :code:`p[1][firstname]` pour le prénom, etc.


Autres membres du ménage
------------------------

Ces informations sont à préfixer de :code:`a` suivi d'un index pour l'entrée. Les index doivent être des entiers incrémenté de la valeur 1 et débuter à 0.


Exemple:

.. code-block::

   a[0][name]=dupont&a[0][firstname]=Yoan&a[0][link]=child&a[1][name]=dupont&a[1][firstname]=isabelle&a[1][link]=child



.. list-table:: Paramètres *Autres membres du ménage*
   :header-rows: 1

   * - Nom du paramètre
     - Information transmise
     - Exemple éventuel
     - Requis (R) ou optionnel (O)
   * - name
     - Nom de famille du membre du ménage
     -
     - R
   * - firstname
     - prénom du membre du ménage
     - 
     - R
   * - birthdate
     - date de naissance du membre du ménage, au format ISO8601
     - :code:`a[0][birthdate]=2014-08-27` pour le 27 août 2014
     - O
   * - gender
     - genre du membre du ménage. A choisir dans les mêmes valeurs que pour les personnes concernées.
     -
     - R
   * - link
     - Lien de parenté avec la personne concernée. A choisir parmi les codes décrits plus bas.
     - 
     - O
   * - sitfam
     - Situation familiale
     - A choisir parmi la même liste que pour les personnes concernées
     - 
     - O

Liste des codes "liens de parenté"
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Les codes sont les suivants :

- :code:`child` enfant
- :code:`gchild`: petit-enfant
- :code:`childwoj`: enfant recueilli **sans** jugement 
- :code:`childwj`: enfant recueilli **avec** jugement 
- :code:`neph`: neveu ou nièce
- :code:`brosis`: frère ou soeur
- :code:`parent`: parent
- :code:`gparent`: grand parent
- :code:`other`: autre


================================
Réponse du système d'information
================================

En mode :code:`GET`, aucune réponse ne sera renvoyée à Immoware.

En mode :code:`POST`, le système d'information répondra avec les informations suivantes:

En cas de succès:
-----------------

- Code de statut :code:`201` 
- contenu:

.. code-block:: json

   {
      "redirect-url": "https://chill.amli.asso.fr/redirect/zArk85esdeg",
      "extid": "e268265a-4483-11e8-8292-b328867b92bb",
      "expiration": "2018-04-20T12:30:00Z"
   }

Le champ :code:`redirect-url` contiendra l'URL de redirection vers laquelle rediriger l'utilisateur d'Immoware.

Le champ :code:`extid` contiendra un identifiant unique qui permettra de suivre l'évolution du mandatement (pour le suivi de la procédure). Il sera de type texte, maximum 255 caractères.

Le champ :code:`expiration` contiendra la date à laquelle le lien expirera. 

En cas d'erreur
---------------

Le SI Amli renverra un status code :code:`400`. 

Dans le cas où le quota d'envoi sera dépassé
--------------------------------------------

Le SI Amli renverra un status code :code:`429` (voir section "sécurité").

Sécurité
--------

L'envoi d'une requête ne modifiera pas les informations enregistrée dans le SI Amli, tant que l'utilisateur ne les validera pas. Pour éviter les requêtes multiples, un quota de 20 requêtes par heure sera introduit. Au delà, le SI AMLI renverra une erreur :code:`429`.

Par simplicité, nous n'introduirons pas d'authentification; mais si cela est techniquement possible pour immoware, nous sommes preneurs.

Retour des informations vers Immoware
=====================================

A déterminer.
