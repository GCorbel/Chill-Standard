<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\DebugBundle\DebugBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Chill\MainBundle\ChillMainBundle(),
            new Chill\CustomFieldsBundle\ChillCustomFieldsBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Chill\PersonBundle\ChillPersonBundle(),
            new Chill\ReportBundle\ChillReportBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new Chill\ActivityBundle\ChillActivityBundle(),
            new Chill\TaskBundle\ChillTaskBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Chill\DocStoreBundle\ChillDocStoreBundle(),
            new ChampsLibres\AsyncUploaderBundle\ChampsLibresAsyncUploaderBundle(),
            new OldSound\RabbitMqBundle\OldSoundRabbitMqBundle()
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();            
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');        
    }
    
    public function getCacheDir()
    {
        return \sys_get_temp_dir().'/app/cache/'.$this->getEnvironment();
    }
    
    public function getLogDir()
    {
        return \sys_get_temp_dir().'/app/logs';
    }
}
