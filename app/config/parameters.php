<?php
$container->setParameter('locale', (isset($_ENV['LOCALE'])) ? $_ENV['LOCALE'] : 'fr' );
// database :
$container->setParameter('database_host', (isset($_ENV['DATABASE_HOST'])) ? $_ENV['DATABASE_HOST'] : 'db' );
$container->setParameter('database_name', (isset($_ENV['DATABASE_NAME'])) ? $_ENV['DATABASE_NAME'] : 'postgres' );
$container->setParameter('database_user', (isset($_ENV['DATABASE_USER'])) ? $_ENV['DATABASE_USER'] : 'postgres' );
$container->setParameter('database_port', (isset($_ENV['DATABASE_PORT'])) ? $_ENV['DATABASE_PORT'] : 5432 );
// redis
$container->setParameter('redis_host', (isset($_ENV['REDIS_HOST'])) ? $_ENV['REDIS_HOST'] : 'redis' );
$container->setParameter('redis_port', (isset($_ENV['REDIS_PORT'])) ? $_ENV['REDIS_PORT'] : '6379' );
$container->setParameter('redis_url', sprintf('redis://%s:%d', $container->getParameter('redis_host'),
    (int) $container->getParameter('redis_port')));
// gelf (for logging)
$container->setParameter('gelf_host', (isset($_ENV['GELF_HOST'])) ? $_ENV['GELF_HOST'] : 'gelf' );
$container->setParameter('gelf_port', (isset($_ENV['GELF_PORT'])) ? $_ENV['GELF_PORT'] : '12201' );
