#!/bin/sh

# stop script if a step fails
set -e

# create a temporary user
groupadd -g ${INSTALL_GID} installgroup
useradd -u ${INSTALL_ID} -g ${INSTALL_GID} installuser

echo 'install composer'

EXPECTED_SIGNATURE=$(curl -o /dev/stdout --silent https://composer.github.io/installer.sig)
su installuser -c "php -r \"copy('https://getcomposer.org/installer', 'composer-setup.php');\""
ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
    >&2 echo 'ERROR: Invalid installer signature'
    rm composer-setup.php
    exit 1
fi

su installuser -c "php composer-setup.php"
rm composer-setup.php

echo ''
echo 'composer installed successfully'
echo ''
echo 'install deps'
echo ''

apt update && apt -y --no-install-recommends install wget gnupg  \
   && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc |  apt-key add - \
   && echo "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main" > /etc/apt/sources.list.d/pgdg.list \
   && apt update \
   && apt -y --no-install-recommends install \
      libicu-dev  \
      g++ \
      libzip-dev \
      libfreetype6-dev \
      postgresql-server-dev-10 \
      libpng-dev \
      libjpeg62-turbo-dev \
      git \
      zip \
      unzip \
   && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
   && docker-php-ext-install -j$(nproc) gd \
   && docker-php-ext-install intl pdo_pgsql mbstring zip bcmath sockets exif 

su installuser -c "php -d memory_limit=2G composer.phar install --no-scripts --ignore-platform-reqs"

echo ''
echo 'deps installed sucessfully'
echo ''


