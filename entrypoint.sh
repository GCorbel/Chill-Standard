#!/bin/bash

#immediatly exit if a command fails:
set -e

# waiting for the database to be ready
while ! timeout 1 bash -c "cat < /dev/null > /dev/tcp/${DATABASE_HOST}/${DATABASE_PORT}"
do  
  echo "$(date) : waiting one second for database"; 
  sleep 1; 
done

echo "$(date) : the database is ready";

#migrate
#php /var/www/app/bin/console doctrine:migrations:migrate --no-interaction --env=prod

{ \
  echo "[www]"; \
  echo ""; \
  echo "user=${PHP_FPM_USER}"; \
  echo "group=${PHP_FPM_GROUP}"; \
} > /usr/local/etc/php-fpm.d/zz-user.conf

#prepare cache
php /var/www/app/app/console --env=prod cache:clear --no-warmup
chgrp ${PHP_FPM_GROUP} /tmp/app/cache -R && chmod g+rw /tmp/app/cache -R
chgrp ${PHP_FPM_GROUP} /tmp/app/logs  -R && chmod g+rw /tmp/app/logs  -R

exec "${@}"

