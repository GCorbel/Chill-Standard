const Encore = require('@symfony/webpack-encore');
const fs = require('fs');

Encore
  .setOutputPath('web/build/')

  .setPublicPath('/build')

  // main page
  .addEntry('main', './app/Resources/assets/main.js')

  // login page
  .addEntry('login', './vendor/chill-project/main/Resources/public/modules/login_page/index.js')

  .enableSassLoader()

  .autoProvidejQuery()

  .enableSourceMaps(!Encore.isProduction())

  .cleanupOutputBeforeBuild()
  
  .enableVersioning()
;


// Add the assets from the Chill bundles
const chillBundles = ['main', 'person', 'report', 'activity', 'custom-fields', 'task'];
let chillEntries = [];

for (let ii in chillBundles) {
  let path = `./vendor/chill-project/${chillBundles[ii]}/chill.webpack.config.js`;
  if (fs.existsSync(path)) {
      chillEntries.push(path);
  }
}

// configure docStore
configure = require('./vendor/chill-project/chill-doc-store/chill.webpack.config.js');
configure(Encore);

config = Encore.getWebpackConfig();
config.entry.chill = chillEntries;

// export the final configuration
module.exports = config;
